<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:output method="html" encoding="utf-8" indent="yes" omit-xml-declaration="yes"/>

   <xsl:template match="/">
      <xsl:apply-templates/>
   </xsl:template>

   <xsl:template match="head">
      <h3><xsl:value-of select="."/></h3>
   </xsl:template>

   <xsl:template match="p">
      <p><xsl:apply-templates/></p>
   </xsl:template>

   <xsl:template match="epigraph">
      <blockquote><xsl:apply-templates/></blockquote>
   </xsl:template>

   <xsl:template match="lg">
      <p><xsl:apply-templates/></p>
   </xsl:template>

   <xsl:template match="l">
      <xsl:apply-templates/><br/>
   </xsl:template>

   <xsl:template match="foreign">
      <span><xsl:apply-templates/></span>
   </xsl:template>

   <xsl:template match="hi[@rend='italics']">
      <em><xsl:apply-templates/></em>
   </xsl:template>

   <xsl:template match="pb"/>

   <xsl:template match="table">
      <table><xsl:apply-templates/></table>
   </xsl:template>

   <xsl:template match="row">
      <tr><xsl:apply-templates/></tr>
   </xsl:template>

   <xsl:template match="cell">
      <td><xsl:apply-templates/></td>
   </xsl:template>

</xsl:stylesheet>
