<?php
defined('TYPO3_MODE') || die();

$boot = function ($_EXTKEY) {

    /* ===========================================================================
        Register an "image gallery" layout
    =========================================================================== */
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['file_list']['templateLayouts'][] = [
        'Image Gallery', // You may use a LLL:EXT: label reference here of course!
        'MyGallery',
    ];

};

$boot($_EXTKEY);
unset($boot);
