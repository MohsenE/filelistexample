<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'My Gallery',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'fluid_styled_content' => '9.5.0-9.5.99',
            'rte_ckeditor' => '9.5.0-9.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'alaki',
    'author_email' => 'alaki@a.b',
    'author_company' => 'alaki',
    'version' => '1.0.0',
];
